package com.wefine.coding

import com.zaxxer.hikari.HikariDataSource
import mu.KLogging
import org.jetbrains.exposed.spring.SpringTransactionManager
import org.jetbrains.exposed.sql.*
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.context.support.ResourceBundleMessageSource
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.stereotype.Controller
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.transaction.annotation.Transactional
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor
import org.springframework.web.servlet.i18n.SessionLocaleResolver
import org.springframework.web.servlet.view.script.ScriptTemplateConfigurer
import org.springframework.web.servlet.view.script.ScriptTemplateViewResolver
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import javax.annotation.Resource
import javax.script.CompiledScript

@SpringBootApplication
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}

@Configuration
@EnableTransactionManagement
class Exposed {
    companion object : KLogging()

    @Bean
    fun transactionManager(dataSource: HikariDataSource): SpringTransactionManager {
        return SpringTransactionManager(dataSource)
                .also {
                    logger.info { "=== USE SQL datasource: user=${dataSource.username} url=${dataSource.jdbcUrl}" }
                }
    }
}

object Customers : Table("customer") {
    val id = long("id").autoIncrement().primaryKey()
    val name = varchar("name", 64)
}

@Service
@Transactional
class ExposedCustomerService : CustomerService {

    override fun findAll(): Collection<Customer> {
        return Customers.selectAll().map {
            Customer(it[Customers.id], it[Customers.name])
        }
    }

    override fun findOne(id: Long): Customer? {
        return Customers.select { Customers.id.eq(id) }
                .map { Customer(it[Customers.id], it[Customers.name]) }
                .firstOrNull()
    }

    override fun insert(c: Customer) {
        Customers.insert { it[Customers.name] = c.name }
    }

    override fun deleteAll() {
        Customers.deleteAll()
    }
}

@Profile("jdbc")
@Service
@Transactional
class JdbcCustomerService(private val jdbcOperations: JdbcOperations) : CustomerService {

    override fun findAll(): Collection<Customer> =
            jdbcOperations.query("SELECT * FROM customer") { rs, _ ->
                Customer(rs.getLong("id"), rs.getString("name"))
            }


    override fun findOne(id: Long): Customer? =
            jdbcOperations.queryForObject("SELECT * FROM customer WHERE id = ?", arrayOf(id)) { rs, _ ->
                Customer(rs.getLong("id"), rs.getString("name"))
            }


    override fun insert(c: Customer) {
        jdbcOperations.update("INSERT INTO customer (name) VALUES (?)", c.name)
    }

    override fun deleteAll() {
        jdbcOperations.update("DELETE FROM customer WHERE 1=1")
    }
}

// 服务接口
interface CustomerService {
    fun findAll(): Collection<Customer>
    fun findOne(id: Long): Customer?
    fun insert(c: Customer)
    fun deleteAll()
}

// 数据实体类
data class Customer(val id: Long = 0, val name: String)

@Configuration
@EnableWebMvc
class WebConfiguration : WebMvcConfigurer {
    @Bean
    fun kotlinScriptConfigurer(): ScriptTemplateConfigurer {
        return ScriptTemplateConfigurer().apply {
            engineName = "kotlin"
            setScripts("scripts/render.kts")
            renderFunction = "render"
            isSharedEngine = false
        }
    }

    @Bean
    fun kotlinScriptViewResolver() = ScriptTemplateViewResolver().apply {
        setPrefix("templates/")
        setSuffix(".kts")
    }

    @Bean
    fun localeResolver() = SessionLocaleResolver().apply {
        setDefaultLocale(Locale.ENGLISH)
    }

    @Bean
    fun localeChangeInterceptor() = LocaleChangeInterceptor()

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(localeChangeInterceptor())
    }

    @Bean
    fun messageSource(): MessageSource = ResourceBundleMessageSource().apply {
        setBasenames("i18n")
        setDefaultEncoding("UTF-8")
    }

    @Bean
    fun cache() = ConcurrentHashMap<String, CompiledScript>()
}

@Controller
class IndexController {
    @Resource
    lateinit var customerService: CustomerService

    @GetMapping("/")
    fun render(model: Model): String {
        model.addAttribute("customers", customerService.findAll())

        return "index"
    }
}