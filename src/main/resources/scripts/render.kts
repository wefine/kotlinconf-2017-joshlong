import org.springframework.beans.factory.getBean
import org.springframework.context.support.ResourceBundleMessageSource
import org.springframework.web.servlet.view.script.RenderingContext
import javax.script.ScriptContext
import javax.script.ScriptEngineManager
import javax.script.SimpleBindings

fun render(template: String, model: Map<String, Any>, renderingContext: RenderingContext): String {
    val engine = ScriptEngineManager().getEngineByName("kotlin")
    val bindings = SimpleBindings(model)
    val messageSource = renderingContext.applicationContext.getBean<ResourceBundleMessageSource>()

    bindings.put("i18n", { code: String -> messageSource.getMessage(code, null, renderingContext.locale) })
    bindings.put("include", { path: String -> renderingContext.templateLoader.apply("templates/$path.kts") })

    engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE)
    return engine.eval(template) as String
}