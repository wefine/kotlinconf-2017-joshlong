package com.wefine.coding

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import javax.annotation.Resource

@RunWith(SpringRunner::class)
@SpringBootTest
class ApplicationTests {

    @Resource
    lateinit var customerService: CustomerService

    @Test
    fun test_01_insert() {
        arrayOf("John", "Mina", "Kate")
                .map { Customer(name = it) }
                .forEach { customerService.insert(it) }
    }

    @Test
    fun test_02_findAll() {
        customerService.findAll().forEach(::println)
    }

    @Test
    fun test_03_deleteAll() {
        customerService.deleteAll()
    }
}
